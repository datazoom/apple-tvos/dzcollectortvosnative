//
//  DZCollectorTvOSNative.swift
//  DZCollectorTvOSNative
//
//  Created by Vuk Simovic on 2.3.22..
//

import Foundation
import AVFoundation
import MediaPlayer
import AVKit
#if !os(macOS)
import UIKit
import TVUIKit
#endif

import DZCollectorTvOSBase

@objc open class DZNativeCollector: NSObject  {
    public static let shared = DZNativeCollector()
   
    //Flag For Flux Data
    public var fluxDataFlag = true
    
    //Flag For DataZoom Internal Automation Purpose
    public var flagForAutomationOnly = Bool()
    
    @objc open func initCollector(configID:String, url:String) {
        DZCollectorTvOSNative.shared.createCollector(configID: configID, url: url)
    }
    
    @objc open func createPlayerWithUrl(videoUrl: String) -> AVPlayer {
        return DZCollectorTvOSNative.shared.createPlayerWithUrl(videoUrl: videoUrl)
    }
    
    @objc open func initPlayer(playerInstance:AVPlayer, playerControllerInstance: AVPlayerViewController, adTag: String?) {
        DZCollectorTvOSNative.shared.initPlayer(playerInstance: playerInstance, playerControllerInstance: playerControllerInstance, adTag: adTag)
    }
    
    @objc open func createAndInitPlayer(playerControllerInstance: AVPlayerViewController, videoUrl: String, adTag: String?, onCompletion: ((Error?) -> Void)?){
        DZCollectorTvOSNative.shared.createAndInitPlayer(playerControllerInstance: playerControllerInstance, videoUrl: videoUrl, adTag: adTag, onCompletion: { error in onCompletion?(error)
        })
    }
    
    
    // Tracking Authorisation
    @objc open func askForTrackingAuthorization() {
        DZCollectorTvOSNative.shared.askConsent()
    }
    
    
    //MARK:- CUSTOM EVENTS AND METADATA
    @objc open func customEvent(eventName: String?,metadata:[String:Any]?) {
        DZCollectorTvOSNative.shared.triggerCustomEvent(name: eventName, metadata: metadata)
    }
    
    //MARK:- CUSTOM METADATA
    @objc open func initCustomMetadata(_ playerMetadata:[String:Any]?, _ sessionMetadata:[String:Any]?, _ customMetadata:[String:Any]?) {
        DZCollectorTvOSNative.shared.initCustomMetadata(playerMetadata, sessionMetadata, customMetadata)
    }
    
    // Player custom metadata
    @objc open func getPlayerCustomMetadata() -> [String:Any]? {
        return DZCollectorTvOSNative.shared.getPlayerCustomMetadata()
    }
    
    @objc open func setPlayerCustomMetadata(_ playerMetadata:[String:Any]?) {
        DZCollectorTvOSNative.shared.setPlayerCustomMetadata(playerMetadata)
    }
    
    @objc open func clearPlayerCustomMetadata() {
        DZCollectorTvOSNative.shared.clearPlayerCustomMetadata()
    }
    
    // Session custom metadata
    @objc open func getSessionCustomMetadata() -> [String:Any]? {
        return DZCollectorTvOSNative.shared.getSessionCustomMetadata()
    }
    
    @objc open func setSessionCustomMetadata(_ sessionMetadata:[String:Any]?) {
        DZCollectorTvOSNative.shared.setSessionCustomMetadata(sessionMetadata)
    }
    
    @objc open func clearSessionCustomMetadata() {
        DZCollectorTvOSNative.shared.clearSessionCustomMetadata()
    }
    
    // Custom metadata
    @objc open func getCustomMetadata() -> [String:Any]? {
        return DZCollectorTvOSNative.shared.getCustomMetadata()
    }
    
    @objc open func setCustomMetadata(_ customMetadata:[String:Any]?) {
        DZCollectorTvOSNative.shared.setCustomMetadata(customMetadata)
    }
    
    @objc open func clearCustomMetadata() {
        DZCollectorTvOSNative.shared.clearCustomMetadata()
    }
    
}
