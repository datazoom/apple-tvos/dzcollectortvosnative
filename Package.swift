// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "DZCollectorTvOSNative",
    platforms: [.tvOS(.v14)],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "DZCollectorTvOSNative",
            targets: ["DZCollectorTvOSNative"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(url: "https://github.com/ashleymills/Reachability.swift", .branch("master")),
        .package(url: "https://gitlab.com/datazoom/apple-tvos/DZCollectorTvOSBase.git", .branch("main")),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "DZCollectorTvOSNative",
            dependencies: [
                .product(name: "Reachability", package: "Reachability.swift"),
                .product(name: "DZCollectorTvOSBase", package: "DZCollectorTvOSBase"),
            ]),
        .testTarget(
            name: "DZCollectorTvOSNativeTests",
            dependencies: ["DZCollectorTvOSNative"]),
    ],
    swiftLanguageVersions: [.v5]
)
